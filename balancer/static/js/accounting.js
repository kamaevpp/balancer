var app = angular.module('accounting', ['ui.bootstrap'])
    .config(['$locationProvider', function($locationProvider) {
         $locationProvider.html5Mode(true);
    }]);
angular.element(document).ready(function(){
    angular.bootstrap(angular.element('[ng-controller=donats_ctrl]'), ['accounting']);
    angular.element(window).on('popstate', function(e){
        console.log(e);
    })
});

var invite_modal_ctrl = function($scope, $modalInstance, $http, $filter) {
    $scope.invited_users = INVITED;
    $scope.ACCESS_TYPES = ACCESS_TYPES;

    $scope.get_users = function(val){
       return $http({method: 'GET', url: '/api/search_user/', params: {starts: val}})
            .then(function(data){
                var res = [],
                    filtered = $filter('filter')(data.data, function(v) {
                        return $filter('filter')(
                            $scope.invited_users,
                            function(u) { return u.id === v.id; }
                        ).length === 0;
                    });
                // should I use underscore.js?
                angular.forEach(filtered, function(v) {
                    v.cancelable = true;
                    v.modified = true;
                    res.push(v);
                });
                return res;
            });
    };

    $scope.cancel_invite = function(user) {
        // should I use underscore.js?
        $scope.invited_users = $filter('filter')($scope.invited_users, function(v) {return v.id !== user.id});
    };

    $scope.disinvite = function(user) {
        $http.post('/api/accounting/'+ACCOUNTING+'/disinvite/', [user.id]);
        $scope.invited_users = $filter('filter')($scope.invited_users, function(v) {return v.id !== user.id});
    };

    $scope.ok = function() {
        var api_params = {};
        angular.forEach($filter('filter')($scope.invited_users, {modified: true}), function(val) {
            api_params[val.id] = val.access.id;
        });
        $http.post('/api/accounting/'+ACCOUNTING+'/invite/', api_params)
            .success(function(data){
                angular.forEach(data, function(user){
                    $scope.invited_users = $filter('filter')($scope.invited_users, function(v) {return v.id !== user.id});
                });
            })
            .error(function(data, status){
                // todo
                console.log(status + ':' + data)
            });
        $modalInstance.close();
    };
    $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
    };
};

app.controller('donats_ctrl', function ($scope, $http, $filter, $modal, $location) {
    "use strict";
    $scope.to_unixtimestamp = function (dt) {
        if(typeof(dt) === 'undefined')
            return null;
        if(dt === null)
            return null;
        return dt.getTime() / 1000;
    };

    $scope.from_date_opened = false;
    $scope.to_date_opened = false;

    $scope.datepickerOptions = {
        'show-weeks': false,
        'starting-day': 1
    };

    // fixme: 4 identical functions
    $scope.open_from = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.to_date_opened = false;
        $scope.from_date_opened=!$scope.from_date_opened;
    };
    $scope.open_to = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.from_date_opened = false;
        $scope.to_date_opened=!$scope.to_date_opened;
    };

    $scope.open_date = function($event, donat) {
        $event.preventDefault();
        $event.stopPropagation();

        donat.until_opened = false;
        donat.date_opened = !donat.date_opened; }
    $scope.open_until = function($event, donat) {
        $event.preventDefault();
        $event.stopPropagation();

        donat.date_opened = false;
        donat.until_opened = !donat.until_opened;
    };

    $scope.acc_config = {
        list_id: ACCOUNTING,
        show_disabled: SHOW_DISABLED,
        start: START,
        stop: STOP,
        tags: TAGS,
        include_empty_tag: false
    };
    $scope.pagination = {
        max_size: 5,
        total: 0,
        page: 1,
        per_page: PER_PAGE
    };


    $scope.get_list = function () {
        var real_params = angular.copy($scope.acc_config);
        angular.extend(real_params, {
            start: $scope.to_unixtimestamp(real_params.start),
            stop: $scope.to_unixtimestamp(real_params.stop),
            limit: $scope.pagination.per_page,
            offset: ($scope.pagination.page - 1) * $scope.pagination.per_page
        });
        return $http({method: "GET", url: "/api/donation/", params: real_params})
            .then(function (promise_info) {
                var search = angular.copy($scope.acc_config);
                $scope.donats = promise_info.data.data;
                $scope.pagination.total = promise_info.data.count;
                angular.forEach($scope.donats, $scope.extend_donat);

                angular.extend(search, {
                    page: $scope.pagination.page,
                    start: $scope.acc_config.start === null ? '' : $filter('date')($scope.acc_config.start, 'yyyy-MM-dd'),
                    stop: $scope.acc_config.stop === null ? '' : $filter('date')($scope.acc_config.stop, 'yyyy-MM-dd')
                });
                $location.search(search);
            });
    };
    $scope.get_list().then(function(){
        $scope.pagination.page = PAGE;
    });

    $scope.extend_donat = function (val, idx) {
        angular.extend(val, {
            js_date: new Date(val.date * 1000),
            edit_mode: false,
            edit_schedule: false,
            idx: idx,
            total: function () {
                var size = val.disabled ? 0 : val.size,
                    is_last = val.idx + 1 === $scope.donats.length;
                return size + (is_last ? 0 : $scope.donats[val.idx + 1].total());
            },
            errors: null,
            old: null,
            date_opened: false,
            until_opened: false,
            js_until: val.until === null ? null : new Date(val.until * 1000)
        });
    };

    $scope.edit = function (d) {
        d.old = angular.copy(d);
        d.edit_schedule = false;
        d.edit_mode = true;
    };
    $scope.remove = function (d) {
        var need_to_delete = window.confirm('Really delete?');
        if (!need_to_delete)
            return;
        $http.delete("/api/donation/" + d.id + '/').success(function () {
            $scope.donats.splice($scope.donats.indexOf(d), 1);
            angular.forEach($scope.donats, function(val, idx) {
                val.idx = idx;
            });
        }).error(function(data, status){
            if (status === 400)
                $scope.errors = data;
        });
    };

    $scope.commit = function(d) {
        var sender = null;
        if (d.id === null)
            sender = function(params) {
                params.list_id = ACCOUNTING;
                return $http.post("/api/donation/", params);
            };
        else
            sender = function(params) {
                params.list_id = ACCOUNTING;
                return $http.put("/api/donation/"+ d.id+'/', params);
            };
        sender(d).success(function(data){
            angular.extend(d, data);
            if(d.old.date !== d.date || d.old.period !== d.period )
                $scope.get_list();
            d.old = null;
            d.errors = null;
            d.edit_mode = false;
        })
        .error(function(data, status) {
            if (status === 400)
                d.errors = data;
            else
                $scope.errors = data;
        });
    };
    $scope.rollback = function(d) {
        if (d.id === null)
        {
            $scope.donats.splice(d.idx, 1);
        }
        else
        {
            angular.extend(d, d.old);
            d.old = null;
            d.errors = null;
        }
    };

    $scope.add_row = function() {
        var new_donat = angular.copy(DONATION_STRUCTURE);
        $scope.donats.push(new_donat);
        $scope.extend_donat(new_donat, $scope.donats.length-1);
        new_donat.edit_mode = true;
    };

    $scope.remove_tag = function(tags, tag) {
        tags.splice(tags.indexOf(tag), 1);
    };

    $scope.get_tags = function(val){
        return $http({method: 'GET', url: '/api/search_tag/', params: {starts: val}})
            .then(function(data){
                var res = [];
                angular.forEach(data.data, function(item){res.push(item.name);});
                return res;
        });
    };

    // invite
    $scope.open_invite_modal = function(){
        $modal.open({
            templateUrl: 'invite_modal',
            controller: invite_modal_ctrl,
            size: 'lg'
        });
    };
});
