import itertools

from flask.views import MethodView

from .. import *
from balancer.forms import AccountingForm
from balancer.models import Accounting


class AccountingController(MethodView):
    decorators = (login_required, convert_to_apierror, returns_json)

    def __detailed_info(self, list_id):
        accounting = Accounting.get(list_id=list_id)
        return accounting.permissions

    # read
    def get(self, accounting_id=None):
        # returns list of accounting shared with current user.
        if accounting_id:
            return self.__detailed_info(accounting_id)
        return list(itertools.chain(
            (x.view(access_type='read') for x in g.user.readonly_shared.all()),
            (x.view(access_type='write') for x in g.user.writeonly_shared.all()),
            (x.view(access_type='full') for x in g.user.moderable_shared.all()),
            (x.view(access_type='owned') for x in g.user.created)))

    # create
    def post(self):
        data = AccountingForm(**request.json)
        if data.validate():
            res = Accounting(g.user, data.name.data)
            res.save()
            return res
        return data.errors

    # update
    def put(self, accounting_id):
        data = AccountingForm(**request.json)
        if data.validate():
            res = Accounting.get(list_id=accounting_id)
            res.name = data.name.data
            res.save()
            return res
        return data.errors

    # delete
    def delete(self, accounting_id):
        acc = Accounting.get(list_id=accounting_id)
        if g.user == acc.owner:
            acc.delete()
            return None
        else:
            raise Forbidden('Only owner can delete accounting.')