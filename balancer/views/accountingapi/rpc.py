from .. import *
from balancer.models import *


def rpc_decorators(route):
    @wraps(route)
    def result_decorator(method):
        @wraps(method)
        def decorated(*args, **kwargs):
            f = returns_json(method)
            f = convert_to_apierror(f)
            f = login_required(f)
            f = app.route(route, methods=['POST'])(f)
            return f(*args, **kwargs)
        return decorated
    return result_decorator


@app.route('/api/accounting/<int:list_id>/invite/', methods=['POST'])
@login_required
@convert_to_apierror
@returns_json
def invite(list_id):
    accounting = Accounting.get(list_id=list_id)
    data = request.json
    if not hasattr(data, 'items'):
        raise IncorrectParameters('Invitations should be dict of user: access.')
    errors = []
    for user_id, access_type in data.items():
        # fixme: add StandardDAOMixin to AccessType?
        if access_type == READ_ACCESS:
            access = AccessType().read
        elif access_type == WRITE_ACCESS:
            access = AccessType().write
        elif access_type == FULL_ACCESS:
            access = AccessType().full
        else:
            # todo: add additional user info
            errors.append({'user': user_id, 'access': access_type, 'error_message': 'Incorrect access type'})
            continue
        try:
            g.user.manage_permissions(accounting=accounting, user=User.get_first(id=user_id), user_access=access,
                                      force_update=g.user.has_access(accounting, AccessType().full))
        except DoesNotExists:
            errors.append({'user': user_id, 'access': access_type, 'error_message': 'No such user'})
    if errors:
        return errors
    return None


@app.route('/api/accounting/<int:list_id>/disinvite/', methods=['POST'])
@login_required
@convert_to_apierror
@returns_json
def disinvite(list_id):
    data = request.json
    for user_id in data:
        try:
            Permission.get(list_id=list_id, user_id=user_id).delete()
        except DoesNotExists:
            # simply ignore
            pass
    return None