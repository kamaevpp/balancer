# standard
import logging
LOGGER = logging.getLogger(__name__)

# flask
from flask import render_template, request, session, redirect, flash, url_for, g, current_app

# project
from .models import User
from . import Auth


@Auth.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.jinja2', title='login')
    elif request.method == 'POST':
        username, password = request.form.get('username', None), request.form.get('password', None)
        check_result = User.check_password(username, password)
        if isinstance(check_result, User):
            session['username'] = username
            g.user = check_result
            flash('Successfully logged in as {}'.format(username), 'success')
            return redirect(request.args.get('next',
                                             url_for(current_app.config.get('LOGIN_DEFAULT_NEXT_ENDPOINT'))))
        LOGGER.debug('Error while login: {}'.format(check_result))
        flash(check_result, 'danger')
        return render_template('login.jinja2', title='login')


@Auth.route('/logout/')
def logout():
    username = session.pop('username', None)
    g.user = None
    next_url = url_for(current_app.config.get('LOGIN_DEFAULT_NEXT_ENDPOINT'))
    if username:
        flash("User '{user}' logged out. "
              "Click <a href={login}>here</a> to login again"
              .format(user=username,
                      login=url_for('.login', next=next_url)), 'success')
    return redirect(next_url)
