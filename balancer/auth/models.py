__author__ = 'kammala'

import crypt
from .. import db

METHOD = crypt.METHOD_SHA256


class User(db.Model):
    __tablename__ = 'users'

    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(51), unique=True)
    password = db.Column(db.String(255))
    salt = db.Column(db.String(100))
    first_name = db.Column(db.String(25))
    last_name = db.Column(db.String(25))
    superuser = db.Column(db.Boolean, default=False)

    def __init__(self, username, password, first_name=None, last_name=None):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        # no plaintext passwords
        self.create_password(password)

    def create_password(self, pswd):
        """Creates POSIX crypted password."""
        self.password, self.salt = self.__crypt_password(pswd)

    def set_names(self, **kwargs):
        if 'first' in kwargs:
            self.first_name = kwargs.get('first')
        if 'last' in kwargs:
            self.last_name = kwargs.get('last')

    @classmethod
    def __crypt_password(cls, pswd, salt=None):
        if not pswd:
            raise ValueError('Empty password not allowed')
        if not salt:
            salt = crypt.mksalt(METHOD)
        return crypt.crypt(pswd, salt), salt

    @classmethod
    def check_password(cls, name, pswd):
        """Validate user's password

        Returns error string or user object
        """
        if not name or not pswd:
            return 'Empty password or username'
        user = cls.get_first(username=name)
        if not user:
            return 'No such user: "{}"'.format(name)
        else:
            return user if cls.__crypt_password(pswd, user.salt)[0] == user.password else 'Invalid password'

    @classmethod
    def validate_password(cls, name, pswd):
        """Validate user's password

        Returns True or False
        """
        return isinstance(cls.check_password(name, pswd), cls)

    @classmethod
    def get_first(cls, username=None, id=None):
        """Returns first user with specified name or id. If it is not exist, returns None"""
        return cls.query.filter_by(username=username).first() if username else cls.query.filter_by(user_id=id).first()
        
    def __repr__(self):
        return '<{_class}> {name}'.format(_class=self.__class__.__name__, name=self.username)