from setuptools import setup, find_packages

#todo: add manage script to setup
setup(
    name='balancer',
    version='0.1b',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    scripts=['manage.py'],
    install_requires=[
        'Flask>=0.7',
        'psycopg2',
        'python-dateutil',
        'sqlalchemy',
        'flask-sqlalchemy',
    ]
)